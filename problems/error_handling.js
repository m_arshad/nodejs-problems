const axios = require('axios');
const mongoose = require('mongoose');
const ErrorLog = require('../models/errorLog');

async function fetchDataFromAPI(apiEndpoint) {
    try {
        const response = await axios.get(apiEndpoint);
        return response.data;
    } catch (error) {
        await ErrorLog.create({ message: `API request failed: ${error.message}` });
        throw new Error('Failed to fetch data.');
    }
}

module.exports = { fetchDataFromAPI };
