const express = require('express');
const jwt = require('jsonwebtoken');

const app = express();
const secretKey = process.env.SECRET_KEY;

function authenticateToken(req, res, next) {
    const token = req.header('Authorization');
    if (!token) return res.status(401).json({ error: 'Access denied' });

    jwt.verify(token, secretKey, (err, user) => {
        if (err) return res.status(403).json({ error: 'Invalid token' });

        req.user = user;
        next();
    });
}

app.use(express.json());

app.get('/protected', authenticateToken, (req, res) => {
    res.json({ message: 'Protected resource accessed successfully' });
});

app.post('/login', (req, res) => {
    const { username, password } = req.body;

    if (username === 'user' && password === 'password') {
        const token = jwt.sign({ username }, secretKey);
        res.json({ token });
    } else {
        res.status(401).json({ error: 'Invalid credentials' });
    }
});

module.exports = app;
