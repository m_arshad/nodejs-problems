const express = require('express');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv'); // Import dotenv

const app = express();
const PORT = 3000;
dotenv.config();
const secretKey = process.env.SECRET_KEY;
// Sample user data
const users = [
    { id: 1, username: 'john', password: 'password123' },
    { id: 2, username: 'jane', password: 'pass456' }
];

app.use(express.json());

// Login endpoint
app.post('/login', (req, res) => {
    const { username, password } = req.body;
    const user = users.find(u => u.username === username && u.password === password);

    if (user) {
        // Create and send JWT token
        const token = jwt.sign({ userId: user.id, username: user.username }, secretKey, { expiresIn: '1h' });
        res.json({ token });
    } else {
        res.status(401).json({ error: 'Invalid credentials' });
    }
});

// Protected endpoint
app.get('/protected', authenticateToken, (req, res) => {
    res.json({ message: 'This is a protected endpoint' });
});

// Middleware to verify JWT token
function authenticateToken(req, res, next) {
    const token = req.headers.authorization && req.headers.authorization.split(' ')[1];

    if (!token) {
        return res.status(401).json({ error: 'Token not provided' });
    }

    jwt.verify(token, secretKey, (err, user) => {
        if (err) {
            return res.status(403).json({ error: 'Invalid token' });
        }

        req.user = user;
        next();
    });
}

// Start the server
app.listen(PORT, () => {
    console.log(`Server is running on http://localhost:${PORT}`);
});
