const fs = require('fs');
const path = require('path');
const mongoose = require('mongoose');
const File = require('../models/file');

function listFilesWithExtension(directoryPath, fileExtension) {
    try {
        // Use absolute path
        const absolutePath = path.join(__dirname, directoryPath);

        // Check if the directory exists
        if (!fs.existsSync(absolutePath)) {
            throw new Error(`Directory does not exist: ${absolutePath}`);
        }

        // Use try-catch for readdirSync to handle errors gracefully
        let filesList;
        try {
            filesList = fs.readdirSync(absolutePath)
                .filter(file => path.extname(file) === `.${fileExtension}`);
        } catch (error) {
            throw new Error(`Failed to list files: ${error.message}`);
        }

        // Continue processing files only if there are files
        if (filesList.length > 0) {
            filesList.forEach(async (filename) => {
                await File.create({ filename });
            });

            return filesList;
        } else {
            console.log(`File System Operations - No .${fileExtension} files found in ${absolutePath}`);
            return [];
        }
    } catch (error) {
        console.error(error.message);
        return [];
    }
}

module.exports = { listFilesWithExtension };


