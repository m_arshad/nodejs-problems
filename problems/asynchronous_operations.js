const mongoose = require('mongoose');
const Content = require('../models/content');

async function downloadContents(urls) {
    try {
        const contents = await Promise.all(urls.map(async (url) => {
            const content = `Content from ${url}`;
            await Content.create({ url, content });
            return content;
        }));

        return contents;
    } catch (error) {
        throw new Error(`Failed to download contents: ${error.message}`);
    }
}

module.exports = { downloadContents };
