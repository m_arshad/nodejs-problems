const mongoose = require('mongoose');

const contentSchema = new mongoose.Schema({
    url: String,
    content: String,
});

const Content = mongoose.model('Content', contentSchema);

module.exports = Content;
