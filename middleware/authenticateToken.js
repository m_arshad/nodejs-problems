const dotenv = require('dotenv'); // Import dotenv
const jwt = require('jsonwebtoken');

dotenv.config();
const secretKey = process.env.SECRET_KEY;


function authenticateToken(req, res, next) {
    const token = req.header('Authorization');
    if (!token) return res.status(401).json({ error: 'Access denied' });

    jwt.verify(token, secretKey, (err, user) => {
        if (err) return res.status(403).json({ error: 'Invalid token' });

        req.user = user;
        next();
    });
}

module.exports = authenticateToken;
