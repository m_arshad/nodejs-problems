const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv'); // Import dotenv


const asyncOperations = require('../problems/asynchronous_operations');
const errorHandling = require('../problems/error_handling');
const fileSystemOperations = require('../problems/file_system_operations');
const databaseInteraction = require('../problems/database_interaction');
const authentication = require('../problems/authentication');
const usersRouter = require('./routes/users');
const path = require('path');

// Load environment variables from .env file
dotenv.config();

const app = express();
const PORT = process.env.PORT || 3000;

// MongoDB connection string
const dbURI = process.env.DB_URI;

// Connect to MongoDB
mongoose.connect(dbURI, { useNewUrlParser: true, useUnifiedTopology: true });

// Middleware to parse JSON requests
app.use(express.json());

// Use routers for specific routes
app.use('/users', usersRouter);

// Run Asynchronous Operations
const urls = ['https://example.com/page1', 'https://example.com/page2'];  //just example you can test it with actual data from postman 
asyncOperations.downloadContents(urls)
    .then(contents => {
        console.log('Asynchronous Operations - Downloaded contents:', contents);
    })
    .catch(err => {
        console.error('Asynchronous Operations - Failed to download contents:', err.message);
    });

// Run Error Handling
const apiEndpoint = 'https://api.example.com/data';  //just example you can test it with actual data from postman 
errorHandling.fetchDataFromAPI(apiEndpoint)
    .then(data => {
        if (data) {
            console.log('Error Handling - Data fetched successfully:', data);
        } else {
            console.log('Error Handling - Failed to fetch data.');
        }
    })
    .catch(err => {
        console.error('Error Handling - An unexpected error occurred:', err.message);
    });

// Run File System Operations
const directoryPath = '/path/to/directory';
const fileExtension = 'txt';

// Use the function with the adjusted path
const filesList = fileSystemOperations.listFilesWithExtension(directoryPath, fileExtension);
console.log(`File System Operations - List of .${fileExtension} files:`, filesList);

// Run Database Interaction
databaseInteraction.listen(PORT, () => {
    console.log(`Database Interaction - Server is running on http://localhost:${PORT}`);
});

// Run Authentication
authentication.listen(PORT + 1, () => {
    console.log(`Authentication - Server is running on http://localhost:${PORT + 1}`);
});
